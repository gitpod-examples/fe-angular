import { Component, OnInit } from '@angular/core';
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit  {
  title = 'fe-angular';
  myData: any;
    constructor(private http: HttpClient) {}
    ngOnInit()  {
        this.http.get<any>(environment.backendURL + "/api").subscribe(data => {
            this.myData = data;
        })
    } 
}
